package com.mljr.demo;

/**
 * Author : LI-JIAN
 * Date   : 2016-12-21
 */
public class MyService {

    public int add1(int a, int b) {
        return a + b;
    }

    public int add2(int a, int b) {
        return a + b;
    }

    public int add3(int a, int b) {
        return a + b;
    }

    public int add4(int a, int b) {
        return a + b;
    }

}
