package com.mljr.demo;

/**
 * Author : LI-JIAN
 * Date   : 2016-12-21
 */
public class GitlabCIDemoMain {

    public static void main(String[] args) {
        try {
            int i = 0;
            while (i++ < 100) {
                System.out.println(i);
                Thread.sleep(1000);
            }
            System.out.println("Exit");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
