package com.mljr.demo;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Author : LI-JIAN
 * Date   : 2016-12-21
 */
public class GitlabCiDemoTest {

    MyService myService;

    @Before
    public void init() {
        myService = new MyService();
    }

    @Test
    public void test() {
        Assert.assertEquals(1 + 2, myService.add1(1, 2));
        Assert.assertEquals(1 + 2, myService.add2(1, 2));
        Assert.assertEquals(1 + 2, myService.add3(1, 2));
    }

}
