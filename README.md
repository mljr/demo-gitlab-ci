# Gitlab-CI Demo 

![jdk    ](https://img.shields.io/badge/Jdk-1.7+-blue.svg)


## Builds

* [![Master](https://gitlab.com/mljr/demo-gitlab-ci/badges/master/build.svg)](https://gitlab.com/mljr/demo-gitlab-ci/commits/master) -  master

* [![Master](https://gitlab.com/mljr/demo-gitlab-ci/badges/develop/build.svg)](https://gitlab.com/mljr/demo-gitlab-ci/commits/develop) -  develop

* [![Master](https://gitlab.com/mljr/demo-gitlab-ci/badges/betaC/build.svg)](https://gitlab.com/mljr/demo-gitlab-ci/commits/betaC) -  betaC

* [![Master](https://gitlab.com/mljr/demo-gitlab-ci/badges/lijian/build.svg)](https://gitlab.com/mljr/demo-gitlab-ci/commits/lijian) -  lijian